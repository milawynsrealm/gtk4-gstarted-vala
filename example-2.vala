/*
 * Name: Getting Started - Packing buttons
 * Reference: https://docs.gtk.org/gtk4/getting_started.html#packing-buttons
 * License: LGPL-2.1-or-later
 * Copyright: GTK Development Team
 * Translated to VALA by Lee Schroeder <spaceseel at gmail dot com>
 */

/*
 * To Compile: valac example-2.vala --pkg gtk4
 */
int main(string[] args) {
    /* Create a new application instance */
    var app = new Gtk.Application("org.gtk.example", GLib.ApplicationFlags.FLAGS_NONE);

    app.activate.connect(() => {
        /* Create a new application instance */
        var window = new Gtk.ApplicationWindow(app);

        /* Create a grid to attach the buttons to */
        var grid = new Gtk.Grid();

        /* Gets the first button set up */
        var button = new Gtk.Button.with_label("Button 1");

        /* Set up the window */
        window.set_title("Window");

        /* When the user clicks on the first button, show a message */
        button.clicked.connect(() => {
            stdout.printf("Hello World!\n");
        });

        /* Attach the grid to the main window */
        window.set_child(grid);

        /* Attach button to the grid */
        grid.attach(button, 0, 0, 1, 1);

        /* Create the second button. The first one is replaced
         * after adding it to the grid. */
        button = new Gtk.Button.with_label("Button 2");

        /* When the user clicks on the second button, show a message */
        button.clicked.connect(() => {
            stdout.printf("Hello World 2!\n");
        });

        /* Attach button to the grid */
        grid.attach(button, 1, 0, 1, 1);

        button = new Gtk.Button.with_label("Quit");

        /* When the user clicks on the button, show a message */
        button.clicked.connect(() => {
            window.close();
        });

        /* Attach button to the grid */
        grid.attach(button, 0, 1, 2, 1);

        window.present();
    });

    /* Actually run the application */
    return app.run(args);
}
