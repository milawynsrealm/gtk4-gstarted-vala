/*
 * Name: Getting Started - Basics
 * Reference: https://docs.gtk.org/gtk4/getting_started.html#basics
 * License: LGPL-2.1-or-later
 * Copyright: GTK Development Team
 * Translated to VALA by Lee Schroeder <spaceseel at gmail dot com>
 */

/*
 * To Compile: valac example-0.vala --pkg gtk4
 */
int main(string[] args) {
    /* Create a new application instance */
    var app = new Gtk.Application("org.gtk.example", GLib.ApplicationFlags.FLAGS_NONE);

    /* When the program is first run, do these tasks first */
    app.activate.connect(() => {
        /* Create a new window object */
        var window = new Gtk.ApplicationWindow(app);

        /* Sets the name of the application */
        window.set_title("Window");

        /* Set the initial size of the window when first shown */
        window.set_default_size(200, 200);

        /* Shows the window to the user */
        window.present();
    });

    /* Actually run the application */
    return app.run(args);
}
