/*
 * Name: Getting Started - Hello World
 * Reference: https://docs.gtk.org/gtk4/getting_started.html#hello-world
 * License: LGPL-2.1-or-later
 * Copyright: GTK Development Team
 * Translated to VALA by Lee Schroeder <spaceseel at gmail dot com>
 */

/*
 * To Compile: valac example-1.vala --pkg gtk4
 */
int main(string[] args) {
    /* Create a new application instance */
    var app = new Gtk.Application("org.gtk.example", GLib.ApplicationFlags.FLAGS_NONE);

    /* When the program is first run, do these tasks first */
    app.activate.connect(() => {
        /* Create a new application instance */
        var window = new Gtk.ApplicationWindow(app);

        /* Create a button to click on */
        var button = new Gtk.Button.with_label("Hello World");

        /* Create a box for the button to be attached to */
        var box = new Gtk.Box(VERTICAL, 0);

        /* When the user clicks on the button, show a message */
        button.clicked.connect(() => {
            /* Show a message in the console window */
            stdout.printf("Hello World\n");
        });

        /* Set up the window */
        window.set_title("Window");
        window.set_default_size(200, 200);

        /* Set up the box. The methods here are inherited from Gtk.Widget */
        box.set_halign(CENTER);
        box.set_valign(CENTER);

        /* Sets the child to the box object */
        window.set_child(box);

        /* Attach the button to the box */
        box.append(button);

        /* Shows the window */
        window.present();
    });

    /* Actually run the application */
    return app.run(args);
}
