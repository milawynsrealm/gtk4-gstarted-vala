/*
 * Name: Getting Started - Packing buttons with GtkBuilder
 * Reference: https://docs.gtk.org/gtk4/getting_started.html#packing-buttons-with-gtkbuilder
 * License: LGPL-2.1-or-later
 * Copyright: GTK Development Team
 * Translated to VALA by Lee Schroeder <spaceseel at gmail dot com>
 */

/*
 * To Compile: valac example-3.vala --pkg gtk4
 */
int main(string[] args) {
    /* Create a new application instance */
    var app = new Gtk.Application("org.gtk.example", GLib.ApplicationFlags.FLAGS_NONE);

    /* When the program is first run, do these tasks first */
    app.activate.connect(() => {
        try {
            /* Create a new window object */
            var builder = new Gtk.Builder();

            /* Load the resource file for the window */
            builder.add_from_file("builder.ui");

            /* Get the window set up */
            var window = builder.get_object("window") as Gtk.Window;
            window.set_application(app);

            /* Get the first button from the resource file */
            var button = builder.get_object("button1") as Gtk.Button;

            /* Let the program know what to do when button1 is clicked */
            button.clicked.connect(() => {
                stdout.printf("Hello World 1\n");
            });

            /* Grab the refrerence to button2 */
            button = builder.get_object("button2") as Gtk.Button;

            /* Tell the program what to do when button2 is clicked */
            button.clicked.connect(() => {
                stdout.printf("Hello World 2\n");
            });

            /* Grab the reference to the quit button */
            button = builder.get_object("quit") as Gtk.Button;

            /* Close the program when clicked */
            button.clicked.connect(() => {
                window.close();
            });

            /* Shows the window to the user */
            window.present();

        } catch (Error e) {
            /* If there was a problem loading the UI. Added this to get the
             * compiler to stop complaining about no error case found. */
            stderr.printf("Could not load UI: %s\n", e.message);
        }
    });

    /* Actually run the application */
    return app.run(args);
}
