# GTK 4.0 Getting Started for VALA Developers #

This source code is essentially a VALA version of the GTK 4 Getting Started guide found in the official [documentation](https://docs.gtk.org/gtk4/getting_started.html). Since I have been having trouble finding good resources on creating GTK 4 programs for the VALA programming language, I have been taking the original C code and trying to figure out what the VALA version would be. I have done my best to keep it as close to the original as possible while making it work for VALA, but at least it works as intended.

## Compiling ##

To compile each program, simply use the following command. Be sure to replace **file.vala** with the name of the source file you want to compile.

```Sh
valac file.vala --pkg gtk4
```
